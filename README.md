# Assignment Test

![VueJS & Php Assignment](https://img.shields.io/badge/assignment-vue--js%7Cphp-yellow)

Implementation of a PHP (no frameworks) & pure Vue JS (no Vanilla JS) Google News RSS query interface with language support for English, German and Franch.

![Initial interface](/public/assets/interface-initial.png)

![Queried interface](/public/assets/interface-queried.png)
