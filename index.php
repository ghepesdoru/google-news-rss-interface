<?php
namespace Assignment\Task;

// Ensure PSR-4 compliance by registering an autoloader for our custom namespace
require __DIR__ . '/src/autoloader.php';
spl_autoload_register('AssignmentNamespaceAutoloader');

// Bootstrap application
require __DIR__ . '/src/bootstrap.php';
?>