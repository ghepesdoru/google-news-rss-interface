<?php
namespace Assignment\Task\Controllers\SearchController;

use Assignment\Task\lib\Articles\GoogleNewsProxy\GoogleNewsProxy;
use Assignment\Task\lib\Routing\RouteController\RouteController;

class SearchController extends RouteController
{
    public function handle(): string|false
    {
        // Get the request body and parse it as JSON
        $body = json_decode(file_get_contents('php://input'), true);

        if ($body !== null) {
            // Sanitize the input data
            $keywords = filter_var($body['keywords'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $language = filter_var($body['language'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);

            // If the input is invalid, return a 400 Bad Request response
            if (empty($keywords) || empty($language)) {
                $this->status = 400;

                return false;
            }

            // Request data from the Google News RSS based on received parameters
            $articlesProvider = new GoogleNewsProxy($keywords, $language);
            $articles = $articlesProvider->unwrap();
            $serialisedArticles = [];

            foreach ($articles as $article) {
                array_push(
                    $serialisedArticles,
                    $article->JSON(),
                );
            }

            return json_encode($serialisedArticles);
        }

        return false;
    }
}
?>