<?php
// Ensure an autoloader capable of loading our namespaced imports
function AssignmentNamespaceAutoloader($class)
{
    $prefix = 'Assignment\\Task\\';
    $base_dir = __DIR__ . '/';

    $len = strlen($prefix);

    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $isInterface = str_contains($class, 'Interface');

    // Handle types with the namespace name as being directly exported
    $segments = explode('\\', $relative_class);
    $len = count($segments);

    if ($segments[$len - 1] == $segments[$len - 2]) {
        $segments = array_slice($segments, 0, $len - 1);
    }

    $relative_class = join('\\', $segments);

    // Handle interfaces separately
    if ($isInterface) {
        $relative_class = str_replace('Interface', '.interface', $relative_class);
    }

    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // if the file exists, require it
    if (file_exists($file)) {
        require $file;
    }
}

?>