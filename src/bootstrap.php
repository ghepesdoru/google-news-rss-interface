<?php
use Assignment\Task\Controllers\SearchController\SearchController;
use Assignment\Task\lib\Routing\Router\Router;

// Initialise routing
$router = new Router;

$routes = [
    [
        'POST',
        '/search',
        new SearchController()
    ]
];

// Register routes, bail out immediately if unable to (a more robust and uniform error handling mechanism would be required for production)
foreach ($routes as $route) {
    if (!$router->addRoute($route[0], $route[1], $route[2])) {
        // No matching route case, 404 and bail out
        http_response_code(500);
        echo '500 Unable to register routes';

        exit();
    }
}

// Handle the current request
$router->handleRequest($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
?>