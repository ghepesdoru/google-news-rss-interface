<?php
namespace Assignment\Task\lib\Articles\Article;

use Assignment\Task\lib\Stringifyable\StringifyableInterface\StringifyableInterface;

class Article implements StringifyableInterface
{
    public function __construct(
        private string $link,
        private string $description,
        private string $source,
        private int $published,
    ) {
    }

    public function JSON(): string
    {
        return json_encode(
            array(
                'link' => $this->link,
                'description' => $this->description,
                'source' => $this->source,
                'published' => $this->published,
            )
        );
    }
}
?>