<?php
namespace Assignment\Task\lib\Articles\GoogleNewsProxy;

use Assignment\Task\lib\Articles\Article\Article;


// use GuzzleHttp\Client;
// use GuzzleHttp\Promise;

class GoogleNewsProxy
{
    private array $promises;

    public function __construct(
        private string $keywords,
        private string $language,
    ) {
        // GuzzleHttp method (alternative if composer available)
        // $client = new Client();
        // $this->promises = [];

        // // Add asynchronous requests to the promise array
        // $this->promises['rss'] = $client->getAsync($this->computeUrl());
    }

    public function unwrap()
    {
        // GuzzleHttp method (alternative if composer available)
        // // Wait for all promises to complete
        // $results = Promise\unwrap($this->promises);

        // Get the response body for the RSS feed
        // return $this->extractArticles($results['rss']->getBody());

        return $this->extractArticles(file_get_contents($this->computeUrl()));
    }

    private function computeUrl()
    {
        return 'https://news.google.com/rss/search?q=' . $this->keywords . '&hl=' . $this->language;
    }

    private function extractArticles(string $xml): array
    {
        $articles = [];

        if (!empty($xml)) {
            // Parse the RSS XML using SimpleXML
            $rss = simplexml_load_string($xml);

            // Access the RSS data
            foreach ($rss->channel->item as $item) {
                array_push(
                    $articles,
                    new Article(
                        $item?->link,
                        $item?->description,
                        $item?->source,
                        \DateTime::createFromFormat("D, d M Y H:i:s e", $item?->pubDate)->getTimestamp()
                    )
                );
            }
        }

        return $articles;
    }
}
?>