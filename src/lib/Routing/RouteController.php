<?php
namespace Assignment\Task\lib\Routing\RouteController;

abstract class RouteController
{
    public function __construct(
        protected int $status = 200,
        protected string $contentType = 'application/json',
    ) {
    }


    /**
     * Main route handler
     *
     * Can return text, pack-ed bite array or false in case of error
     */
    public abstract function handle(): string|false;

    /**
     * Status code getter
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Content Type getter
     */
    public function getContentType()
    {
        return $this->contentType;
    }
}
?>