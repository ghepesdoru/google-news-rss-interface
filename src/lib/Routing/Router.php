<?php
namespace Assignment\Task\lib\Routing\Router;

use Assignment\Task\lib\Routing\RouteController\RouteController;


/**
 * Basic router able to register routes and handle requests based on exact matches of path segments.
 *
 * This was only added to allow discrimination between the /search API endpoint and static assets serving under /assets endpoint
 */
class Router
{
    private $routes = [
        'GET' => [],
        'POST' => [],
    ];

    /**
     * Register a route controller on specified path and HTTP method
     */
    public function addRoute($method, $path, RouteController $route): bool
    {
        if (array_key_exists($method, $this->routes)) {
            $this->routes[$method][$path] = $route;

            return true;
        }

        return false;
    }

    /**
     * Simple route matching and request handling mechanism
     */
    public function handleRequest($method, $uri)
    {
        // Start output buffering
        ob_start();

        $path = parse_url($uri, PHP_URL_PATH);
        $controller = null;

        if (array_key_exists($method, $this->routes)) {
            // Get the actual controller based on request method
            $methodRoutes = $this->routes[$method];


            if (array_key_exists($path, $methodRoutes)) {
                $controller = $methodRoutes[$path];
            }
        }

        if ($controller === null) {
            $staticAsset = false;

            // Handle root resource serving
            if ($method == 'GET') {
                $file = realpath(__DIR__ . '/../../../public');

                if ($path == '/') {
                    // Serve the base home HTML for root requests
                    $file = $file . '/index.html';

                    $staticAsset = true;
                } else if (str_starts_with($path, '/assets')) {
                    // Static asset serving case
                    $file = $file . '/' . $path;

                    $staticAsset = true;
                }

                if ($staticAsset) {
                    if (file_exists($file)) {
                        // Set the content type
                        $mime_type = mime_content_type($file) ?? 'text/plain';
                        header("Content-Type: $mime_type");

                        // Output the file contents and exit
                        readfile($file);
                        exit;
                    } else {
                        // Missing static resource
                        $staticAsset = false;
                    }
                }
            }

            if (!$staticAsset) {
                // No matching route case, 404 and bail out
                http_response_code(404);
                echo '404 Not Found' . $path; // A custom 404 page would be used in a production environment when dealing with a real router

                exit();
            }
        }

        $processingErrors = ob_get_clean() ?? [];

        // Log any errors or warnings that occurred during the processing phase
        $errors = error_get_last();

        if ($errors !== null && ($errors['type'] === E_WARNING || $errors['type'] === E_ERROR)) {
            $error_msg = sprintf(
                '%s (%s:%s)',
                $errors['message'],
                $errors['file'],
                $errors['line']
            );

            error_log($error_msg);
        }

        // Compute the result
        ob_start();
        $result = $controller->handle();
        $routeErrors = ob_get_clean() ?? [];

        // Render the result
        $status = $controller->getStatus();
        $contentType = $controller->getContentType();

        http_response_code($status);
        header('Content-Type: ' . $contentType);

        if ($controller->getContentType() == 'application/json') {
            // Uniformly format the response
            echo json_encode([
                'status' => $status,
                'success' => $result !== false,
                'result' => $result !== false ? $result : null,
                'errors' => $routeErrors, // Temporary forward the errors for this simple test case, normally a robust error handling should dictate what happens with this - and probably all - errors
            ]);
        } else {
            echo $result;
        }
    }
}
?>