<?php
namespace Assignment\Task\lib\Stringifyable\StringifyableInterface;

interface StringifyableInterface
{
    public function JSON(): string;
}
?>